import $ from 'jquery';
window.$ = $;
/**
 * A set of mixins used by both components.
 */
export default {
    data () {
        return {
            windowWidth : window.innerWidth,
            windowHeight: window.innerHeight,
        };
    },
    computed: {
        /**
         * The viewport indicates a mobile device.
         * @return {Boolean}
         */
        isMobile() {
            return this.windowWidth < 768;
        },
    },
    methods: {
        /**
         * Computes button height for accordion / tab layouts.
         * @return {integer}
         */
        tabButtonHeight() {
            let buttons, length = 1;
            if ( this.isMobile ) {
                buttons = $('.tab-label');
                length  = buttons.length;
            } else {
                buttons = $('.tab-buttons > li');
            }
            return buttons.height() * length;
        },
        /**
         * Sets the innerWidth and innerHeight of viewport and
         * forces tabButtonHeight to recalculate.
         * @return {void}
         */
        updateViewport() {
            this.windowWidth  = window.innerWidth;
            this.windowHeight = window.innerHeight;
            this.tabButtonHeight();
        },
    },
    /**
     * Adds event listener to update viewport dimensions on resize.
     * @return {void}
     */
    mounted() {
        window.addEventListener('resize', this.updateViewport);
    }
};
